package com.example.runningapp;
import org.junit.Test;

import snatik.polygon.Point;

import static org.junit.Assert.*;

public class PointUnitTest {
    
    @Test
    public void calculate_distance(){
        Point point = new Point(0,0);
        Point point2 = new Point(2,2);
        Point point3 = new Point(4,5);

        assertEquals(point.getDistanceFromPoint(point2, 100, 100), 2.82, 0.1);
        assertEquals(point2.getDistanceFromPoint(point3, 100, 100), 3.6, 0.1);
        assertEquals(point3.getDistanceFromPoint(point3, 100, 100), 0, 0.00001);
        assertEquals(point3.getDistanceFromPoint(point, 100, 100), 6.403, 0.001);
        assertEquals(point.getDistanceFromPoint(point3, 100, 100), 6.403, 0.001);
    }
}
