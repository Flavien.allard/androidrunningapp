package com.example.runningapp;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MyUtilsTest {

    @Test
    public void convert_degrees_to_meters(){
        double x1 = 180,
                x2 = 90,
                x3 = 45,
                x4 = 12.5,
                x5 = 150;

        assertEquals(myUtils.convertToMeter(x1), 19980000, 0.01);
        assertEquals(myUtils.convertToMeter(x2), 9990000, 0.01);
        assertEquals(myUtils.convertToMeter(x3), 4995000, 0.01);
        assertEquals(myUtils.convertToMeter(x4), 1387500, 0.01);
        assertEquals(myUtils.convertToMeter(x5), 16650000, 0.01);
    }
}
