package com.example.runningapp;

import org.junit.Test;

import java.util.ArrayList;
import snatik.polygon.Point;
import snatik.polygon.Polygon;

public class PolygonTest {

    @Test
    public void containsTest(){
        ArrayList<Point> points = new ArrayList<>();

        points.add(new Point(0,0));
        points.add(new Point(0,1));
        points.add(new Point(1,1));
        points.add(new Point(1,0));
        points.add(new Point(0,0));

        Polygon.Builder squareBuilder = Polygon.Builder();

        for(Point point : points){
            squareBuilder.addVertex(point);
        }

        Polygon square = squareBuilder.build();

        points.clear();

        points.add(new Point(1,-1));
        points.add(new Point(2,0));
        points.add(new Point(1,1));
        points.add(new Point(-1,1));
        points.add(new Point(-2,0));
        points.add(new Point(-1,-1));
        points.add(new Point(1,-1));

        Polygon.Builder hexagonBuilder = Polygon.Builder();

        for(Point point : points){
            hexagonBuilder.addVertex(point);
        }

        Polygon hexagon = hexagonBuilder.build();



        assert(square.contains(new Point(0.5, 0.5)));
        assert(!square.contains(new Point(2,2)));
        assert(square.contains(new Point(0.7,0.2)));

        assert(hexagon.contains(new Point(0, 0)));
        assert(!hexagon.contains(new Point(3,2)));
        assert(hexagon.contains(new Point(-1,0)));
    }
}
