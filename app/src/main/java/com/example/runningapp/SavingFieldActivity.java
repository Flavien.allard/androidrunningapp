package com.example.runningapp;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.Objects;

public class SavingFieldActivity extends AppCompatActivity {

    TextView fieldInfos, fieldNameInput;
    Button saveFieldButton;
    FieldSerializer fieldSerializer;
    PlayField playField;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saving_field);

        final File file = new File(getApplication().getFilesDir().getPath(), FieldSerializer.getFieldsFilename());
        playField = (PlayField) Objects.requireNonNull(getIntent().getExtras()).getSerializable("FIELD");
        if (playField == null) {
            Toast.makeText(getApplicationContext(), "Erreur lors de l'enregistrement du terrain.", Toast.LENGTH_LONG).show();
            startActivity(new Intent(getApplicationContext(), MenuActivity.class));
        }

        initializeGraphicalElements();

        fieldInfos.setText("Veuillez entrer un nom pour votre terrain !\nCelui-ci comporte actuellement " + playField.getPoints().size() + " points.");

        saveFieldButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fieldNameInput.getText() != null && !fieldNameInput.getText().toString().trim().equals("")){
                    try{
                        playField.setName(fieldNameInput.getText().toString());
                        playField.MakePolygon();
                        fieldSerializer = new FieldSerializer(file);
                        fieldSerializer.SerializeField(playField);
                    }catch (RuntimeException ex){
                        Toast.makeText(getApplicationContext(), "Le Terrain doit contenir au moins 3 points !", Toast.LENGTH_LONG).show();
                    }
                    finally {
                        startActivity(new Intent(getApplicationContext(), MenuActivity.class));
                    }
                }
                else
                    Toast.makeText(getApplicationContext(), "Le nom ne peut pas être vide. Veuillez entrer un nom valide !", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void initializeGraphicalElements(){
        // Getting Text Views and Buttons
        fieldInfos = findViewById(R.id.fieldSavingInfosText);
        fieldNameInput = findViewById(R.id.fieldNameInputText);
        saveFieldButton = findViewById(R.id.saveFieldButton);
    }
}