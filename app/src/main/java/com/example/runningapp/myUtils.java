package com.example.runningapp;

import java.util.Random;

import snatik.polygon.Point;
import snatik.polygon.Polygon;

public final class myUtils {

    private static final int DEGREES_TO_M = 111000;

    public static Point generateNextTarget(PlayField playField){
        Polygon fieldAsPolygon = playField.getPolygon();
        Polygon.BoundingBox boundingBox = fieldAsPolygon.get_boundingBox();

        Random r = new Random();

        while (true){
            double low_x = boundingBox.xMin;
            double high_x = boundingBox.xMax;
            double result_x = low_x + (high_x - low_x) * r.nextDouble();

            double low_y = boundingBox.yMin;
            double high_y = boundingBox.yMax;
            double result_y = low_y + (high_y - low_y) * r.nextDouble();

            Point nextTarget = new Point(result_x, result_y);
            if (fieldAsPolygon.contains(nextTarget))
                return nextTarget;
        }
    }

    public static double convertToMeter(double value){
        return value * DEGREES_TO_M;
    }

}
