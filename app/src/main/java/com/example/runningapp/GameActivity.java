package com.example.runningapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationResult;

import java.util.Objects;

import snatik.polygon.Point;
import snatik.polygon.Polygon;

public class GameActivity extends AppCompatActivity {

    // ATTRIBUTES
    LocationCallback locationCallBack;
    TextView latitudeText, longitudeText, timerText, nextTargetLat, nextTargetLon, numTargetReached;
    MyGpsLocator myGpsLocator;
    MyCountDownTimer countDownTimer;
    PlayField playField;
    Point nextTarget, myLocation, lastLocation;
    MyToneGenerator toneGenerator;
    Polygon.BoundingBox boundingBox;
    /* Vibrator vibrator; */

    int targetReached = 0;
    int fieldNum;

    double width, height, validation_range;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        // Initialization
        initializeGraphicalElements();
        initializeGameParameters();

        myLocation = new Point(0,0);

        // Executed function when a new location is detected
        locationCallBack = new LocationCallback(){
            @SuppressLint("SetTextI18n")
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);

                VerifyCountDown();
                UpdateLocationAndUI(locationResult.getLastLocation());

                double distanceFromTarget = myLocation.getDistanceFromPoint(nextTarget, width, height);
                toneGenerator.setInterval((int) distanceFromTarget * 2);

                /* StartVibration(nextTarget, myLocation);*/
                
                // If player in range for target
                if (distanceFromTarget <= validation_range){
                    // TODO: Scoring
                    Toast.makeText(getApplicationContext(), "Cible atteinte", Toast.LENGTH_LONG).show();
                    targetReached++;
                    nextTarget = myUtils.generateNextTarget(playField);
                    updateTargetUI(nextTarget);
                }
                lastLocation = myLocation;
            }
        };
        startGame();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // If returned code enables GPS usage
        if (requestCode == 99) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                Toast.makeText(this, "Permission accordée !", Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(this, "Cette application ne peut fonctionner sans ces permissions", Toast.LENGTH_SHORT).show();
        }
    }

    private void initializeGraphicalElements(){

        // Setting TextViews
        latitudeText = findViewById(R.id.latitudeText);
        longitudeText = findViewById(R.id.longitudeText);
        timerText = findViewById(R.id.gameTimerText);
        nextTargetLat = findViewById(R.id.nextTargetLatitude);
        nextTargetLon = findViewById(R.id.nextTargetLongitude);
        numTargetReached = findViewById(R.id.targetReachedText);
    }

    @SuppressLint("SetTextI18n")
    private void updateTargetUI(Point nextTarget){

        // Updating nextTarget location UI
        nextTargetLat.setText(Double.toString(nextTarget.getX()));
        nextTargetLon.setText(Double.toString(nextTarget.getY()));

        // Updating UI for number of targets reached
        numTargetReached.setText(getString(R.string.nombre_de_cibles_atteintes) + targetReached);
    }

    private void initializeGameParameters(){

        // Retrieving chosen Field
        playField = (PlayField) Objects.requireNonNull(getIntent().getExtras()).getSerializable("FIELDNUM");
        fieldNum = (int) Objects.requireNonNull(getIntent().getExtras()).getSerializable("NUM");

        // Creating Beep media player
        toneGenerator = new MyToneGenerator();

        // Creating GpsLocator and Game timer
        myGpsLocator = new MyGpsLocator(this);
        //countDownTimer = new MyCountDownTimer(300000, timerText);
        countDownTimer = new MyCountDownTimer(60000, timerText);

        // Setting up field distances
        boundingBox = playField.getPolygon().get_boundingBox();
        width = boundingBox.yMax - boundingBox.yMin;
        height = boundingBox.xMax - boundingBox.xMin;

        validation_range = Math.sqrt(width + height);

        /*vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);*/
    }

    private void startGame(){
        // Starting geo-location requests
        Toast.makeText(getApplicationContext(), "La partie commencera lorsque la première cordonnée est obtenue !", Toast.LENGTH_LONG).show();
        nextTarget = myUtils.generateNextTarget(playField);
        myGpsLocator.startLocationRequests(this, locationCallBack);
        updateTargetUI(nextTarget);
    }

    private void VerifyCountDown(){

        // Verifying if game is finished
        if (!countDownTimer.isRunning()){
            if (countDownTimer.getTimeLeftMs() == 0){
                Toast.makeText(getApplicationContext(), "C'est fini", Toast.LENGTH_LONG).show();

                myGpsLocator.stopRequests(locationCallBack);
                toneGenerator.stopBeeps();

                /* vibrator.cancel(); */

                // Starting Ending Activity
                Intent endingActivity = new Intent(getApplicationContext(), EndingActivity.class);
                endingActivity.putExtra("NUMTARGETREACHED", targetReached);
                endingActivity.putExtra("NUM", fieldNum);
                startActivity(endingActivity);
            }
            else{
                toneGenerator.playBeep();
                countDownTimer.startCountdown();
            }
        }
    }

    /*
    private void StartVibration(Point target, Point actualLocation){
        double angle = Math.atan2(Math.abs(target.getY() - actualLocation.getY()), Math.abs(target.getX() - actualLocation.getX())) * 180 / Math.PI;

        Log.d("ANGLE", String.valueOf(angle));
        vibrator.vibrate(0);
    }*/

    @SuppressLint("SetTextI18n")
    private void UpdateLocationAndUI(Location location){
        // Updating player location
        myLocation.setX(myUtils.convertToMeter(location.getLatitude()));
        myLocation.setY(myUtils.convertToMeter(location.getLongitude()));

        // UI updates about player location
        latitudeText.setText(Double.toString(myLocation.getX()));
        longitudeText.setText(Double.toString(myLocation.getY()));
    }
}