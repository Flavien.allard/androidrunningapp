package com.example.runningapp;

import android.location.Location;

import java.io.Serializable;
import java.util.ArrayList;

import snatik.polygon.Polygon;
import snatik.polygon.Point;

public class PlayField implements Serializable {

    // ATTRIBUTES
    private Polygon polygon;
    private final ArrayList<Point> points = new ArrayList<>();
    private String name = "default";
    private int highScore = 0;

    // Adding a point to collection
    public void Add(Point point){
        point.setX(point.getX());
        point.setY(point.getY());
        points.add(point);
    }

    // Converting collection into a Polygon
    public void MakePolygon(){

        // Closing the polygon
        points.add(points.get(0));

        Polygon.Builder builder = Polygon.Builder();

        for(Point point : points){
            builder.addVertex(point);
        }
        polygon = builder.build();
    }

    public Polygon getPolygon() {
        return polygon;
    }

    public ArrayList<Point> getPoints() {
        return points;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHighScore() {
        return highScore;
    }

    public void setHighScore(int highScore) {
        this.highScore = highScore;
    }
}
