package com.example.runningapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

public class EndingActivity extends AppCompatActivity {

    int fieldNum, score;

    Button saveButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ending);

        TextView scoreText = findViewById(R.id.textView3);

        score = (int) Objects.requireNonNull(getIntent().getExtras()).getSerializable("NUMTARGETREACHED");
        fieldNum = (int) Objects.requireNonNull(getIntent().getExtras()).getSerializable("NUM");

        scoreText.setText(String.format("Vous avez atteint %d cibles !", score));

        saveButton = findViewById(R.id.saveScoreButton);

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveHighScore();
            }
        });
    }

    private void saveHighScore(){
        FieldSerializer fieldSerializer = new FieldSerializer(new File(getApplication().getFilesDir().getPath(), FieldSerializer.getFieldsFilename()));

        try {
            ArrayList<PlayField> playfields = new ArrayList<>(fieldSerializer.ReadFields());
            PlayField playfield = playfields.get(fieldNum);
            if (playfield.getHighScore() < score){
                playfields.remove(fieldNum);
                playfield.setHighScore(score);
                playfields.add(fieldNum, playfield);

                fieldSerializer.WriteFields(playfields);
            }
        } catch (IOException | ClassNotFoundException e) {
            Toast.makeText(getApplicationContext(), "Une erreur est survenue lors de la sauvegarde !", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
        finally {
            startActivity(new Intent(getApplicationContext(), MenuActivity.class));
        }
    }
}