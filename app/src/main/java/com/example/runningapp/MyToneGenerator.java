package com.example.runningapp;

import android.media.AudioManager;
import android.media.ToneGenerator;

public class MyToneGenerator {

    boolean mustBeep = true;
    int interval = 1000;
    ToneGenerator toneGenerator = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);

    final int DURATION = 100;

    public void playBeep(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    while (mustBeep){
                        Thread.sleep(interval);
                        toneGenerator.startTone(ToneGenerator.TONE_PROP_BEEP, DURATION);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public void stopBeeps(){
        mustBeep = false;
    }

    public int getInterval() {
        return interval;
    }

    public void setInterval(int interval) {
        if (interval > 10000)
            this.interval = 10000;
        else
            this.interval = interval;
    }
}
