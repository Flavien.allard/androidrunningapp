package com.example.runningapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        // Getting button references
        Button playButton = findViewById(R.id.playButton);
        Button fieldsButton = findViewById(R.id.fieldsButton);
        Button parametersButton = findViewById(R.id.paramatersButton);

        // Setting Listeners
        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), ChooseFieldActivity.class);
                startActivity(intent);
            }
        });

        fieldsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), FieldActivity.class);
                startActivity(intent);
            }
        });

        parametersButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), ParametersActivity.class);
                startActivity(intent);
            }
        });

        //FieldSerializer.DeleteFieldsFile(getApplicationContext());
    }
}