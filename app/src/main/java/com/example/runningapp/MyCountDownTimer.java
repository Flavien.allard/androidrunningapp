package com.example.runningapp;

import android.annotation.SuppressLint;
import android.os.CountDownTimer;
import android.widget.TextView;

public class MyCountDownTimer {

    private CountDownTimer countDownTimer;
    private final TextView textView;
    private long timeLeftMs;
    private boolean isRunning = false;
    final int MS_TO_S = 1000;

    public MyCountDownTimer(long timeLeft, TextView textView){
        timeLeftMs = timeLeft + MS_TO_S;
        this.textView = textView;
    }

    public void startCountdown(){
        countDownTimer = new CountDownTimer(timeLeftMs, 1000) {
            @Override
            public void onTick(long l) {
                timeLeftMs = l;
                showTimeLeft();
            }

            @SuppressLint("SetTextI18n")
            @Override
            public void onFinish() {
                timeLeftMs = 0;
                stopTimer();
                textView.setText("00:00");
            }
        }.start();
        isRunning = true;
    }

    public void stopTimer(){
        countDownTimer.cancel();
        isRunning = false;
    }

    @SuppressLint("SetTextI18n")
    public void showTimeLeft(){
        int minutesLeft = (int) timeLeftMs / (60 * MS_TO_S);
        int secondsLeft = (int) timeLeftMs % (60 * MS_TO_S) / 1000;
        String minutesString, secondsString;

        if (minutesLeft < 10)
            minutesString = "0" + minutesLeft;
        else
            minutesString = Integer.toString(minutesLeft);

        if (secondsLeft < 10)
            secondsString = "0" + secondsLeft;
        else
            secondsString = Integer.toString(secondsLeft);

        textView.setText(minutesString + ":" + secondsString);
    }

    public boolean isRunning(){
        return isRunning;
    }

    public long getTimeLeftMs() {
        return timeLeftMs;
    }
}
