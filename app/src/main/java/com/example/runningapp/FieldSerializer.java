package com.example.runningapp;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class FieldSerializer {

    private static final String fieldsFilename = "fields";
    private final File fieldsFile;

    public FieldSerializer(File file){ fieldsFile = file; }

    public void SerializeField(PlayField field) {

        ArrayList<PlayField> playFields = new ArrayList<>();

        if(fieldsFile.exists() && !fieldsFile.isDirectory()) {
            try{
                Log.d("FILE", "File exists");
                // Reading file
                playFields = ReadFields();

                // Adding created Field
                playFields.add(field);

                // Writing new Field to file
                WriteFields(playFields);
                Log.d("FILE", String.format("File written to %s",fieldsFile.getAbsolutePath()));

            }catch (IOException | ClassNotFoundException ex) {
                ex.printStackTrace();
            }
        }
        // If file did not exist
        else{
            try{
                playFields.add(field);
                fieldsFile.createNewFile();
                Log.d("FILE", "File created");
                WriteFields(playFields);
                Log.d("FILE", String.format("File written to %s",fieldsFile.getAbsolutePath()));
            }catch (IOException ex){
                Log.d("FILE", "cant open the file");
                ex.printStackTrace();
            }
        }
    }

    // To write new field in fields file
    public void WriteFields(ArrayList<PlayField> playFields) throws IOException {
        Log.d("FILE", "File opened");
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(fieldsFile));
        objectOutputStream.writeObject(playFields);
        objectOutputStream.close();
        Log.d("FILE", "File closed");
    }

    // To read content of fields file
    public ArrayList<PlayField> ReadFields() throws IOException, ClassNotFoundException {

        // Getting Stream from file
        ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(fieldsFile));
        ArrayList<PlayField> playFields = (ArrayList<PlayField>) objectInputStream.readObject();
        objectInputStream.close();

        // Returning what has been read
        return playFields;
    }

    // Deleting fields file
    public static void DeleteFieldsFile(Context context){
        File file = new File(context.getFilesDir(), fieldsFilename);
        file.delete();
    }

    public static String getFieldsFilename() {
        return fieldsFilename;
    }
}
