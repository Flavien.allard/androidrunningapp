package com.example.runningapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class ChooseFieldActivity extends AppCompatActivity {

    FieldSerializer fieldSerializer;
    ArrayList<PlayField> playFields;

    ListView fieldsListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_field);
        setContentView(R.layout.activity_choose_field);

        initializeGraphicalElements();

        File file = new File(getApplication().getFilesDir().getPath(), FieldSerializer.getFieldsFilename());
        fieldSerializer = new FieldSerializer(file);

        try {
            playFields = fieldSerializer.ReadFields();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        ArrayList<String> fieldNames = new ArrayList<>();

        if (playFields != null){
            for (PlayField playField : playFields)
                fieldNames.add(playField.getName());


            // Populating listView
            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, fieldNames);
            fieldsListView.setAdapter(arrayAdapter);

            fieldsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Intent gameIntent = new Intent(getApplicationContext(), StatsActivity.class);
                    gameIntent.putExtra("FIELDNUM", playFields.get(i));
                    gameIntent.putExtra("NUM", i);
                    startActivity(gameIntent);
                }
            });
        }
        else
            Toast.makeText(getApplicationContext(), "Pas de terrain enregistré !", Toast.LENGTH_LONG).show();
    }

    private void initializeGraphicalElements(){
        fieldsListView = findViewById(R.id.fieldListView);
    }
}