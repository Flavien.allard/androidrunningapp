package com.example.runningapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Objects;

import snatik.polygon.Polygon;

public class StatsActivity extends AppCompatActivity {

    TextView nom_terrain, largeur_terrain, longueur_terrain, record_terrain;
    Button startGameButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stats);

        nom_terrain = findViewById(R.id.nom_terrain);
        largeur_terrain = findViewById(R.id.largeur_terrain);
        longueur_terrain = findViewById(R.id.longueur_terrain);
        record_terrain = findViewById(R.id.record_terrain);

        startGameButton = findViewById(R.id.StartGameButton);

        // Retrieving chosen Field
        final PlayField playField = (PlayField) Objects.requireNonNull(getIntent().getExtras()).getSerializable("FIELDNUM");
        final int fieldNum = (int) Objects.requireNonNull(getIntent().getExtras()).getSerializable("NUM");

        Polygon.BoundingBox boundingBox = playField.getPolygon().get_boundingBox();

        nom_terrain.setText(playField.getName());
        largeur_terrain.setText(Math.round(boundingBox.yMax - boundingBox.yMin) + " mètres");
        longueur_terrain.setText(Math.round(boundingBox.xMax - boundingBox.xMin) + " mètres");
        record_terrain.setText(String.valueOf(playField.getHighScore()));

        startGameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent gameIntent = new Intent(getApplicationContext(), GameActivity.class);
                gameIntent.putExtra("FIELDNUM", playField);
                gameIntent.putExtra("NUM", fieldNum);
                startActivity(gameIntent);
            }
        });
    }
}