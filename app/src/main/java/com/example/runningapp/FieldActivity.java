package com.example.runningapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import snatik.polygon.Point;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationResult;

public class FieldActivity extends AppCompatActivity {

    LocationCallback locationCallback;
    MyGpsLocator myGpsLocator;
    PlayField playField = new PlayField();
    TextView lat, lon;

    boolean next = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_field);

        initializeGraphicalElements();

        // New GPS Locator
        myGpsLocator = new MyGpsLocator(this);

        // Callback called when location has changed
        locationCallback = new LocationCallback(){
            @SuppressLint("SetTextI18n")
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);

                // If user hasn't finished
                if (next){
                    Location location = locationResult.getLastLocation();
                    double latitude = myUtils.convertToMeter(location.getLatitude());
                    double longitude = myUtils.convertToMeter(location.getLongitude());

                    playField.Add(new Point(latitude, longitude));
                    lat.setText("Latitude : " + latitude);
                    lon.setText("Longitude : " + longitude);
                }
                // field is bounded
                else{
                    Location location = locationResult.getLastLocation();
                    playField.Add(new Point(myUtils.convertToMeter(location.getLatitude()), myUtils.convertToMeter(location.getLongitude())));

                    Toast.makeText(getApplicationContext(), "Acquisition du dernier point, veuillez patienter !", Toast.LENGTH_LONG).show();

                    Intent savingFieldIntent = new Intent(getApplicationContext(), SavingFieldActivity.class);
                    savingFieldIntent.putExtra("FIELD", playField);
                    myGpsLocator.stopRequests(this);
                    startActivity(savingFieldIntent);
                }
            }
        };

        // Setting up stop button
        findViewById(R.id.button_stop_field).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                next = false;
            }
        });

        // Requesting locations
        myGpsLocator.startLocationRequests(this, locationCallback);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 99) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Permission accordée !", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Cette application ne peut fonctionner sans ces permissions", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void initializeGraphicalElements(){
        // Getting TextViews
        lat = findViewById(R.id.fieldLatitudeText);
        lon = findViewById(R.id.fieldLongitudeText);
    }
}