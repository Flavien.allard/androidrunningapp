package com.example.runningapp;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

public class MyGpsLocator {

    private static final int PERMISSIONS_FINE_LOCATION = 99;
    private final LocationRequest locationRequest;
    private final FusedLocationProviderClient fusedLocationProviderClient;

    // Constructor
    public MyGpsLocator(Activity activity){

        // Setting up Location requests settings
        locationRequest = LocationRequest.create();
        locationRequest.setInterval(1000);
        locationRequest.setFastestInterval(1000);
        locationRequest.setMaxWaitTime(2000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(activity);
    }

    // Starting geo-location requests
    public void startLocationRequests(Activity activity, LocationCallback callback){
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            fusedLocationProviderClient.requestLocationUpdates(locationRequest, callback, null);
            Toast.makeText(activity, "Lancement de l'acquisition des coordonnées.", Toast.LENGTH_LONG).show();
        }
        else{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                activity.requestPermissions(new String[] {Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSIONS_FINE_LOCATION);
            }
        }
    }

    // Stop getting requests
    public void stopRequests(LocationCallback locationCallback){
        fusedLocationProviderClient.removeLocationUpdates(locationCallback);
    }
}
