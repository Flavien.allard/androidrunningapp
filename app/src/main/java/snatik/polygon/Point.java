package snatik.polygon;

import java.io.Serializable;

/**
 * Point on 2D landscape
 *
 * @author Roman Kushnarenko (sromku@gmail.com)</br>
 */
public class Point implements Serializable {

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    private double x;
    private double y;

    public double getDistanceFromPoint(Point target, double width, double height){
        return Math.sqrt(Math.pow(((this.getX() - target.getX()) / width) * 100, 2) + Math.pow(((this.getY() - target.getY()) / height) * 100, 2));
    }

    @Override
    public String toString() {
        return String.format("(%f,%f)", x, y);
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }
}